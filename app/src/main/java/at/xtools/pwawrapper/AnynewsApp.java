package at.xtools.pwawrapper;

import android.app.Application;
import android.util.Log;

import info.guardianproject.apthelper.AptHelper;
import info.guardianproject.netcipher.webkit.WebkitProxy;

public class AnynewsApp extends Application {

    private static boolean aptInitialized = false;

    public static boolean aptInitialized() {
        return aptInitialized;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // APT
        String bridgeline = "obfs4 37.218.247.26:443 no-fingerpring cert=72cefPoNMgI5qFhHTWGvs+LV4jIroE4i/0RyJRLOCGTe9rZTOy5vT2I1QnNEuWkK044SQg iat-mode=0";
        AptHelper.setupApt(AnynewsApp.this, bridgeline, aptInfo -> {
            try {
                WebkitProxy.setProxy(AnynewsApp.class.getName(), AnynewsApp.this, null, aptInfo.getHttpProxyConfig().getHostName(), aptInfo.getHttpProxyConfig().getPort());
                Log.i("#pt", "configured http proxy settings to " + aptInfo.getHttpProxyConfig());
            } catch (Exception e) {
                Log.e("#pt", "could not set webkit proxy", e);
            }
            AnynewsApp.aptInitialized = true;
        });
    }
}
