# Anynews PT

This is prototype Android App for displaying
[Anynews PWA](https://github.com/n8fr8/anynews) over a Pluggable Transport.

This is currently configured to always connect to one specific PT-bridge,
which is running exclusively for testing purposes.

download debug build: [app/debug/app-debug.apk](https://gitlab.com/uniqx/anynews-android/raw/master/app/debug/app-debug.apk)  
(Currently only tested for Android Emulator x86 API 28)
